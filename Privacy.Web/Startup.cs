﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Privacy.Core.Repositories;
using Privacy.Core.Services;
using Privacy.Data.Sql.Repository;
using Privacy.Services;
using Privacy.Web.Bootstraps;
using System;
using static Privacy.Web.Bootstraps.ConfigureAutoMapper;

namespace Privacy.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var connectionString = Configuration.GetConnectionString("PrivateConnection.Key");

            // Add Db configuration
            services.AddDbConfigurations(connectionString);

            // Add Options
            services.AddOptions();

            // Add MVC dependencies
            services.AddMvc();

            // Add Swagger configuration
            services.AddSwaggerConfiguration();

            // Add Cors 
            services.AddCors(x =>
            {
                x.AddPolicy("corsPolicy", p =>
                {
                    p.AllowAnyOrigin();
                    p.AllowAnyHeader();
                    p.AllowAnyMethod();
                });
            });
            // Add Auto Mapper Configurations
            services.AddAutoMapperConfiguration();

            // Add Dependencies Services and Repository
            services.AddScoped<IUserProfileRepository, UserProfileRepository>();
            services.AddScoped<IUserProfileService, UserProfileService>();
            services.AddScoped<IContactRepository, ContactRepository>();
            services.AddScoped<IContactService, ContactService>();
            services.AddScoped<IAccountService, AccountService>();
            //services.AddDependencies();

            services.AddMvc(options =>
            {
                options.Filters.Add(typeof(ModelValidationFilter));
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseStaticFiles();

            app.UseCors("corsPolicy");

            app.UseSwagger();
            app.UseSwaggerUI(x =>
            {
                x.SwaggerEndpoint("/swagger/v1/swagger.json", "Privacy System Server");
                x.RoutePrefix = string.Empty;
            });

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
