﻿using AutoMapper;
using Privacy.Core.Models;
using Privacy.Web.ViewModels;

namespace Privacy.Web.Helpers
{
    public class WebDataMapperProfile : Profile
    {
        public WebDataMapperProfile()
        {
            CreateMap<UserProfileSaveViewModel, UserProfile>()
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForMember(x => x.Name, opt => opt.Ignore())
                .ReverseMap();

            CreateMap<SaveContactViewModel, ContactProfile>()
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForMember(x => x.Name, opt => opt.Ignore())
                .ReverseMap();

            CreateMap<RegistrationViewModel, UserProfile>()
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForMember(x => x.Name, opt => opt.Ignore())
                .ReverseMap();
        }
    }
}
