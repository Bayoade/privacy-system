﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Privacy.Web.Helpers
{
    public class IocontainerProvider
    {
        public static IContainer ApplicationContainer { get; private set; }

        public static void Register(IContainer container)
        {
            ApplicationContainer = container;
        }

        public static void RunInLifetimeScope(Action<ILifetimeScope> operation, Action<ContainerBuilder> configurationAction = null)
        {
            if (configurationAction == null)
            {
                configurationAction = (ContainerBuilder b) => { };
            }

            using (var scope = ApplicationContainer.BeginLifetimeScope(configurationAction))
            {
                operation(scope);
            }
        }

        public static async Task RunInLifetimeScopeAsync(Func<ILifetimeScope, Task> operation, Action<ContainerBuilder> configurationAction = null)
        {
            if (configurationAction == null)
            {
                configurationAction = (ContainerBuilder b) => { };
            }

            using (var scope = ApplicationContainer.BeginLifetimeScope(configurationAction))
            {
                await operation(scope);
            }
        }
    }
}
