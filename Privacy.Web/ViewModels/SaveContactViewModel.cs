﻿using System;

namespace Privacy.Web.ViewModels
{
    public class SaveContactViewModel
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string MiddleName { get; set; }

        public string Email { get; set; }

        public string Address { get; set; }

        public string PhoneNumber { get; set; }

        public DateTime DateOfBirth { get; set; }

        public string EmergencyNumber { get; set; }

        public string Website { get; set; }

        public string Name { get; set; }
    }
}
