﻿using Microsoft.AspNetCore.Mvc;
using Privacy.Core.Services;
using System;
using System.Threading.Tasks;

namespace Privacy.Web.Controllers
{
    [Route("api/userProfiles")]
    [ApiController]
    public class UserProfilesController : ControllerBase
    {
        private readonly IUserProfileService _userService;

        public UserProfilesController(IUserProfileService userService)
        {
            _userService = userService;
        }

        /// <summary>
        /// Returns the User profile with the parameter id 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<IActionResult> GetUserProfile(Guid id)
        {
            var userProfile = await _userService.GetUserProfileAsync(id);

            return Ok(userProfile);
        }

        /// <summary>
        /// Returns a list of User profile information
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetUserProfiles()
        {
            var userProfiles = await _userService.GetAllUserProfilesAsync();

            return Ok(userProfiles);
        }
    }
}