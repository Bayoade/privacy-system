﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Privacy.Core.Models;
using Privacy.Core.Services;
using Privacy.Web.ViewModels;

namespace Privacy.Web.Controllers
{
    [Route("api/userprofiles")]
    [ApiController]
    public class ContactProfilesController : ControllerBase
    {
        private readonly IContactService _contactService;

        public ContactProfilesController(IContactService contactService)
        {
            _contactService = contactService;
        }

        /// <summary>
        /// Add contact to user
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="saveContactViewModel"></param>
        /// <returns></returns>
        [HttpPost("{userId:guid}/contacts")]
        public async Task<IActionResult> AddContact(Guid userId, [FromBody] SaveContactViewModel saveContactViewModel)
        {
            var contactProfile = Mapper.Map<ContactProfile>(saveContactViewModel);
            var newId = await _contactService.AddContactToUserProfileAsync(userId, contactProfile);

            return Ok(newId);
        }

        /// <summary>
        /// Remove temporarily the contact from user
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPut("{userId:guid}/contacts/delete")]
        public async Task<IActionResult> RemoveContact(Guid userId, Guid id)
        {
            await _contactService.RemoveContactFromUserProfileAsync(userId, id);
            return Ok();
        }

        /// <summary>
        /// returns contact in user profile
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{userId:guid}/contacts/{id:guid}")]
        public async Task<IActionResult> GetContact(Guid userId, Guid id)
        {
            var contactProfile = await _contactService.GetContactToUserProfileAsync(userId, id);
            return Ok(contactProfile);
        }

        /// <summary>
        /// returns all the contact in user profile
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet("{userId:guid}/contacts")]
        public async Task<IActionResult> GetAllContacts(Guid userId)
        {
            var contactProfiles = await _contactService.GetAllUserContactsProfileAsync(userId);
            return Ok(contactProfiles);
        }

        /// <summary>
        /// updates the user profile contact
        /// </summary>
        /// <param name="saveContactViewModel"></param>
        /// <returns></returns>
        [HttpPut("{userId:guid}/contacts/{id:guid}")]
        public async Task<IActionResult> UpdateContact(Guid id, [FromBody] SaveContactViewModel saveContactViewModel)
        {
            var contactProfile = Mapper.Map<ContactProfile>(saveContactViewModel);
            contactProfile.Id = id;
            await _contactService.UpdateContactInUserProfileAsync(contactProfile);

            return Ok();
        }

        /// <summary>
        /// Delete contact permanently 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{userId:guid}/contacts/{id:guid}")]
        public async Task<IActionResult> DeleteContact(Guid userId, Guid id)
        {
            await _contactService.DeleteUserContactAsync(userId, id);
            return Ok();
        }
    }
}