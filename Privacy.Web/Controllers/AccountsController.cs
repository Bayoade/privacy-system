﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Privacy.Core.Models;
using Privacy.Core.Services;
using Privacy.Web.ViewModels;
using System;
using System.Threading.Tasks;

namespace Privacy.Web.Controllers
{
    [Route("api/accounts")]
    [ApiController]
    public class AccountsController : ControllerBase
    {
        private readonly IUserProfileService _userService;
        private readonly IAccountService _accountService;

        public AccountsController(
            IUserProfileService userService,
            IAccountService accountService)

        {
            _userService = userService;
            _accountService = accountService;
        }

        /// <summary>
        /// Add a new User Information
        /// </summary>
        /// <param name="registrationViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> AddUserProfile([FromBody] RegistrationViewModel registrationViewModel)
        {
            var newId = await _userService.AddUserProfileAsync(Mapper.Map<UserProfile>(registrationViewModel));

            return Ok(newId);
        }

        /// <summary>
        /// Update User Information
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userProfileViewModel"></param>
        [HttpPut("{id:guid}")]
        public async Task<IActionResult> UpdateUserProfile(Guid id, [FromBody] UserProfileSaveViewModel userProfileViewModel)
        {
            var userProfileModel = Mapper.Map<UserProfile>(userProfileViewModel);
            userProfileModel.Id = id;
            await _userService.UpdateUserAsync(userProfileModel);

            return Ok();
        }

        /// <summary>
        /// Delete the user profile from the system
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteUserProfile(Guid id)
        {
            await _userService.RemoveUserAsync(id);

            return Ok();
        }

        /// <summary>
        /// Authenticate login for logging in user
        /// </summary>
        /// <param name="loginView"></param>
        /// <returns></returns>
        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody]LoginViewModel loginView)
        {
            var userNamedId = await _accountService.AuthenticateLogin(Mapper.Map<LoginProfile>(loginView));

            return Ok(userNamedId);
        }

        /// <summary>
        /// gets the user name of the logged in user
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet("{userId:guid}/userName")]
        public async Task<IActionResult> GetUserName(Guid userId)
        {
            var userName = await _accountService.GetUserNameAsync(userId);
            return Ok(userName);
        }
    }
}