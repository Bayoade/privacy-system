﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection;
using Privacy.Core.Services;
using Privacy.Data.Sql.Repository;
using Privacy.Services;
using Privacy.Web.Helpers;
using System;
using System.Linq;
using System.Reflection;

namespace Privacy.Web.Bootstraps
{
    public static class ConfigureDependencies
    {
        public static IServiceProvider AddDependencies(this IServiceCollection services)
        {
            var builder = new ContainerBuilder();

            var dataAccess = Assembly.GetExecutingAssembly();

            //builder.RegisterAssemblyTypes(dataAccess) // find all types in the assembly
            //       .Where(t => t.Name.EndsWith("Repository")) // filter the types
            //       .AsImplementedInterfaces()  // register the service with all its public interfaces
            //       .SingleInstance(); // register the services as singletons

            //builder.RegisterAssemblyTypes(dataAccess)
            //    .Where(t => t.Name.EndsWith("Service"))
            //    .AsImplementedInterfaces()
            //    .SingleInstance();

            builder.RegisterAssemblyTypes(typeof(UserProfileRepository).Assembly)
               .Where(t => t.GetInterfaces().Any(i => i.Name.EndsWith("Repository")))
               .InstancePerLifetimeScope();

            builder.RegisterType<UserProfileService>()
                .As<IUserProfileService>().InstancePerDependency();

            builder.Populate(services);
            
            var container = builder.Build();

            IocontainerProvider.Register(container);

            return new AutofacServiceProvider(container);
        }
    }
}
