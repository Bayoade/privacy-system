﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Privacy.Data.Sql.Context;

namespace Privacy.Web.Bootstraps
{
    public static class ConfigureDbContext
    {
        public static void AddDbConfigurations(this IServiceCollection services, string connectionString)
        {
            services.AddDbContext<PrivacyDbContext>(options =>
                 options.UseSqlServer(connectionString));
        }
    }
}
