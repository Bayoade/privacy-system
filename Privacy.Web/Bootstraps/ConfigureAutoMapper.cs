﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using Privacy.Core;
using Privacy.Data.Sql;
using Privacy.Web.Helpers;

namespace Privacy.Web.Bootstraps
{
    public static class ConfigureAutoMapper
    {
        public static void AddAutoMapperConfiguration(this IServiceCollection services)
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<CoreMapperProfile>();
                cfg.AddProfile<SqlDataMapperProfile>();
                cfg.AddProfile<WebDataMapperProfile>();
            });
        }
    }
}
