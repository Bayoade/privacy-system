﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.IO;
using System.Reflection;

namespace Privacy.Web.Bootstraps
{
    public static class ConfigureSwagger
    {
        public static void AddSwaggerConfiguration(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "Privacy System Application",
                    Description = "A Private System that manages APIs",
                    TermsOfService = string.Empty,
                    Contact = new Contact
                    {
                        Name = "Privacy Co-orperation System",
                        Email = "privacy.system@mail.com",
                        Url = "https://twitter.com/privacysystem"
                    }
                });

                // Set the comments path for the Swagger JSON and UI.

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);

            });
        }
    }
}
