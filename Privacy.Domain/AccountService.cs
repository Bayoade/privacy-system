﻿using Privacy.Core.Models;
using Privacy.Core.Repositories;
using Privacy.Core.Services;
using System;
using System.Threading.Tasks;
using static Privacy.Core.Helpers.CryptoExtension;

namespace Privacy.Services
{
    public class AccountService : IAccountService
    {
        private readonly IUserProfileRepository _userProfileRepository;

        public AccountService(IUserProfileRepository userProfileRepository)
        {
            _userProfileRepository = userProfileRepository;
        }
        public async Task<NamedId<Guid>> AuthenticateLogin(LoginProfile model)
        {
            var userProfile = await _userProfileRepository.FindUserByEmail(model.Email);
            var hashPassword = model.Password.ToSha256(userProfile.Salt, 7825);

            if (hashPassword != userProfile.Password)
            {
                return null;
            };

            return new NamedId<Guid>
            {
                Id = userProfile.Id,
                Name = userProfile.Name
            };
        }

        public async Task<string> GetUserNameAsync(Guid userId)
        {
            var userProfile = await _userProfileRepository.GetUserProfileAsync(userId);
            return userProfile.Name;
        }
    }
}
