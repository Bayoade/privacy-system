﻿using Privacy.Core.Models;
using Privacy.Core.Repositories;
using Privacy.Core.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using static Privacy.Core.Helpers.CryptoExtension;

namespace Privacy.Services
{
    public class UserProfileService : IUserProfileService
    {
        private readonly IUserProfileRepository _userProfileRepository;
        private readonly IContactRepository _contactRepository;

        public UserProfileService(
            IUserProfileRepository userProfileRepository, 
            IContactRepository contactRepository)

        {
            _userProfileRepository = userProfileRepository;
            _contactRepository = contactRepository;
        }

        public Task<UserProfile> GetUserProfileAsync(Guid id)
        {
            return _userProfileRepository.GetUserProfileAsync(id);
        }

        public Task<IList<UserProfile>> GetAllUserProfilesAsync()
        {
            return _userProfileRepository.GetAllUserProfilesAsync();
        }

        public async Task RemoveUserAsync(Guid userId)
        {
            await _contactRepository.DeleteAllUserContacts(userId);
            await _userProfileRepository.RemoveUserProfileAsync(userId);
        }

        public Task UpdateUserAsync(UserProfile userProfile)
        {
            return _userProfileRepository.UpdateUserProfileAsync(userProfile);
        }

        public async Task<Guid> AddUserProfileAsync(UserProfile userProfile)
        {
            userProfile.Id = Guid.NewGuid();
            var salt = GenerateSalt(32);
            userProfile.Salt = salt;
            userProfile.Password = userProfile.Password.ToSha256(salt, 7825);

            await _userProfileRepository.AddUserProfileAsync(userProfile);

            return userProfile.Id;
        }
    }
}
