﻿using Privacy.Core.Helpers;
using Privacy.Core.Models;
using Privacy.Core.Repositories;
using Privacy.Core.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Privacy.Services
{
    public class ContactService : IContactService
    {
        private readonly IContactRepository _contactRepository;

        public ContactService(IContactRepository contactRepository)
        {
            _contactRepository = contactRepository;
        }

        public async Task<Guid> AddContactToUserProfileAsync(Guid userId, ContactProfile contactProfile)
        {
            contactProfile.Id = Guid.NewGuid();
            await _contactRepository.AddContactsToUserAsync(userId, contactProfile);
            return contactProfile.Id;
        }

        public Task DeleteUserContactAsync(Guid userId, params Guid[] ids)
        {
            return _contactRepository.DeleteUserContactAsync(userId, ids);
        }

        public Task<IList<ContactProfile>> GetAllUserContactsProfileAsync(Guid userId)
        {
            return _contactRepository.GetAllUserContactsAsync(userId);
        }

        public async Task<ContactProfile> GetContactToUserProfileAsync(Guid userId, Guid id)
        {
            var contact = await _contactRepository.GetContactToUserProfileAsync(userId, id, true);

            if (contact.IsNull())
            {
                throw new Exception($"The contact id {id} with userId {userId} is not available");
            }
            return contact;
        }

        public Task RemoveContactFromUserProfileAsync(Guid userId, params Guid[] contactIds)
        {
            return _contactRepository.RemoveContactFromUserAsync(userId, contactIds);
        }

        public Task UpdateContactInUserProfileAsync(ContactProfile contactProfile)
        {
            return _contactRepository.UpdateContactInUserAsync( contactProfile);
        }
    }
}
