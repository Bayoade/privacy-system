﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Privacy.Core.Models;
using Privacy.Core.Repositories;
using Privacy.Data.Sql.Context;
using Privacy.Data.Sql.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Privacy.Core.Helpers.ExtensionMethods;

namespace Privacy.Data.Sql.Repository
{
    public class UserProfileRepository : IUserProfileRepository
    {
        private readonly PrivacyDbContext _dbContext;
        public UserProfileRepository(PrivacyDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public Task AddUserProfileAsync(UserProfile userProfile)
        {
            var user = Mapper.Map<User>(userProfile);
            user.CreatedDate = DateTime.UtcNow;

            _dbContext.UserProfiles.Add(user);

            return _dbContext.SaveChangesAsync();
        }

        public async Task<IList<UserProfile>> GetAllUserProfilesAsync()
        {
            var users = await _dbContext.UserProfiles.ToListAsync();
            return Mapper.Map<IList<UserProfile>>(users);
        }



        public async Task<UserProfile> GetUserProfileAsync(Guid id)
        {
            var user = _dbContext.UserProfiles.Local.FirstOrDefault(x => x.Id == id);

            if (user.IsNull())
            {
                user = await _dbContext.UserProfiles.FirstOrDefaultAsync(x => x.Id == id);
            }
            return Mapper.Map<UserProfile>(user);
        }

        public async Task UpdateUserProfileAsync(UserProfile userProfile)
        {
            var existingUser = await _dbContext.UserProfiles.FirstOrDefaultAsync(x => x.Id == userProfile.Id);

            if (existingUser.IsNull())
            {
                return;
            }

            Mapper.Map(userProfile, existingUser);

            existingUser.Modified = DateTime.UtcNow;

            _dbContext.UserProfiles.Update(existingUser);
            _dbContext.SaveChanges();
        }

        public Task RemoveUserProfileAsync(params Guid[] userIds)
        {
            var users = new List<User>();
            foreach (var id in userIds)
            {
                var user = _dbContext.UserProfiles.Local.FirstOrDefault(x => x.Id == id);

                if (user.IsNull())
                {
                    user = new User { Id = id };
                }

                users.Add(user);
            }
            _dbContext.UserProfiles.AttachRange(users);
            _dbContext.UserProfiles.RemoveRange(users);
            return _dbContext.SaveChangesAsync();
        }

        public async Task<UserProfile> FindUserByEmail(string email)
        {
            var user = await _dbContext.UserProfiles.FirstOrDefaultAsync(x => x.Email == email);

            return Mapper.Map<UserProfile>(user);
        }
    }
}
