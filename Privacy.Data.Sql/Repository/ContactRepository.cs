﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Privacy.Core.Models;
using Privacy.Core.Repositories;
using Privacy.Data.Sql.Context;
using Privacy.Data.Sql.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Privacy.Core.Helpers;

namespace Privacy.Data.Sql.Repository
{
    public class ContactRepository : IContactRepository
    {
        private readonly PrivacyDbContext _dbContext;

        public ContactRepository(PrivacyDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task RemoveContactFromUserAsync(Guid userId, params Guid[] ids)
        {
            var contacts = new List<Contact>();
            foreach (var id in ids)
            {
                var dbContact = _dbContext.Contacts.Local.FirstOrDefault(x => x.Id == id);

                if (dbContact.IsNull())
                {
                    dbContact = await _dbContext.Contacts.FirstOrDefaultAsync(x => x.Id == id);
                    dbContact.Deleted = true;

                    contacts.Add(dbContact);
                }
            }

            _dbContext.Contacts.UpdateRange(contacts);

            await _dbContext.SaveChangesAsync();
        }

        public async Task UpdateContactInUserAsync(ContactProfile contactProfile)
        {
            var contactDb = await _dbContext.Contacts.FirstOrDefaultAsync(x => x.Id == contactProfile.Id);

            Mapper.Map(contactProfile, contactDb);
            contactDb.Modified = DateTime.UtcNow;

            _dbContext.Contacts.Attach(contactDb);

            _dbContext.Contacts.Update(contactDb);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<ContactProfile> GetContactToUserProfileAsync(Guid userId, Guid id, bool allowNull = false)
        {
            var contact = await _dbContext
                .Contacts
                .FirstOrDefaultAsync(x => x.Id == id && x.UserId == userId && !x.Deleted);

            if (contact.IsNull())
            {
                if (allowNull)
                {
                    return null;
                }

                throw new Exception("");
            }

            return Mapper.Map<ContactProfile>(contact);
        }

        public async Task<IList<ContactProfile>> GetAllUserContactsAsync(Guid userId)
        {
            var contacts = await _dbContext.Contacts.Where(x => x.UserId == userId && !x.Deleted ).ToListAsync();

            return Mapper.Map<IList<ContactProfile>>(contacts);
        }

        public Task DeleteUserContactAsync(Guid userId, params Guid[] ids)
        {
            var contacts = new List<Contact>();

            foreach (var contactId in ids)
            {
                var contact = new Contact
                {
                    Id = contactId,
                    UserId = userId
                };

                contacts.Add(contact);
            }

            _dbContext.Contacts.AttachRange(contacts);
            _dbContext.Contacts.RemoveRange(contacts);

            return _dbContext.SaveChangesAsync();
        }

        public Task AddContactsToUserAsync(Guid userId, ContactProfile contactProfile)
        {
            var contact = Mapper.Map<Contact>(contactProfile);
            contact.CreatedDate = DateTime.UtcNow;
            contact.UserId = userId;

            _dbContext.Contacts.Add(contact);

            return _dbContext.SaveChangesAsync();
        }

        public async Task DeleteAllUserContacts(Guid userId)
        {
            var contacts = await _dbContext.Contacts.Where(x => x.UserId == userId).ToListAsync();
            if (contacts.IsNullOrEmpty())
            {
                return;
            }
            _dbContext.Contacts.RemoveRange(contacts);
            await _dbContext.SaveChangesAsync();
        }
    }
}
