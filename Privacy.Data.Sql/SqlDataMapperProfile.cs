﻿using AutoMapper;
using Privacy.Core.Models;
using Privacy.Data.Sql.Entities;

namespace Privacy.Data.Sql
{
    public class SqlDataMapperProfile : Profile
    {
        public SqlDataMapperProfile()
        {
            CreateMap<User, UserProfile>()
                .ForMember(x => x.Name, opt => opt.Ignore())
                .ReverseMap()
                .ForMember(x => x.Modified, opt => opt.Ignore())
                .ForMember(x => x.CreatedDate, opt => opt.Ignore());

            CreateMap<User, User>().ReverseMap();

            CreateMap<Contact, ContactProfile>()
                .ForMember(x => x.Name, opt => opt.Ignore())
                .ReverseMap()
                .ForMember(x => x.Deleted, opt => opt.Ignore())
                .ForMember(x => x.Modified, opt => opt.Ignore())
                .ForMember(x => x.CreatedDate, opt => opt.Ignore());
        }
    }
}
