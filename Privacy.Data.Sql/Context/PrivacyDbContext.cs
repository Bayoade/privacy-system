﻿using Microsoft.EntityFrameworkCore;
using Privacy.Data.Sql.Entities;

namespace Privacy.Data.Sql.Context
{
    public class PrivacyDbContext : DbContext
    {
        public PrivacyDbContext(DbContextOptions<PrivacyDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            var userModelBuilder = modelBuilder
                .Entity<User>()
                .ToTable("Users");

            var contactModelBuilder = modelBuilder
                .Entity<Contact>()
                .ToTable("Contacts");

            contactModelBuilder.HasKey(x => new { x.UserId, x.Id });
        }

        internal DbSet<User> UserProfiles { get; set; }
        internal DbSet<Contact> Contacts { get; set; }
    }
}
