﻿IF NOT EXISTS (SELECT 1 FROM dbo.Users)
BEGIN
	SET IDENTITY_INSERT [dbo].[Users] ON 

	INSERT [dbo].[Users] ([Id], [Address], [Email], [LastName], [FirstName], [MiddleName], [PhoneNumber]) VALUES (N'A3538AF9-D10E-409B-8963-C70B3D3A4D17', N'South Africa', N'dayo@example.com', N'Dayo',N'Adeleye', N'Moses', N'08075883822')
	INSERT [dbo].[Users] ([Id], [Address], [Email], [LastName], [FirstName], [MiddleName], [PhoneNumber]) VALUES (N'1C5B3407-CA63-4D40-918C-7767B9920834', N'Nigeria', N'john.smith@example.com', N'John',N'Smith', N'Meko', N'09032322222')
	INSERT [dbo].[Users] ([Id], [Address], [Email], [LastName], [FirstName], [MiddleName], [PhoneNumber]) VALUES (N'08DD75AF-2531-42FF-98AA-22E577EDF28D', N'Ukraine', N'mike@example.com', N'Ayo',N'Akin', N'Lazoor', N'07038823929')
	
	SET IDENTITY_INSERT dbo.Users OFF
END