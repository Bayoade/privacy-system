﻿CREATE TABLE [dbo].[Contacts]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY, 
	[FirstName] NVARCHAR(256) NOT NULL,
	[LastName] NVARCHAR(256) NOT NULL,
	[MiddleName] NVARCHAR(256) NULL,
	[Email] NVARCHAR(256) NULL,
	[PhoneNumber] NVARCHAR(256) NOT NULL,
	[Address] NVARCHAR(256) NULL,
	[Website] NVARCHAR(256) NULL,
	[EmergencyNumber] NVARCHAR(256) NULL,
	[DateOfBirth] DATETIME2 (7)  NULL DEFAULT sysutcdatetime(),
	[Modified] DATETIME2 (7)  NULL DEFAULT sysutcdatetime(),
	[CreatedDate] DATETIME2 (7)  NULL DEFAULT sysutcdatetime(),
	[Deleted] BIT  DEFAULT ((0)) NULL,
	[UserId] UNIQUEIDENTIFIER NOT NULL, 
    CONSTRAINT [FK_Contacts_Users] FOREIGN KEY ([UserId]) REFERENCES [Users]([Id]),
)
