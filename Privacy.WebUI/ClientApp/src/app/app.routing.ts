import { Routes, RouterModule } from "@angular/router"
import { UserListComponent } from "src/app/components/user/list/index.component";
import { EditUserComponent } from "src/app/components/user/edit/edit.component";
import { LoginComponent } from "src/app/components/auths/login/login.component";
import { SignUpPageComponent } from "src/app/components/auths/signup/signup.page.component";
import { UserDetailsComponent } from "src/app/components/user/userdetails/user-details-component";

const routes : Routes = [
   { path: '', redirectTo: '/login', pathMatch: 'full'},
   // { path : 'index', component: UserListComponent},
   { path : 'edit', component: EditUserComponent },
   { path : 'login', component: LoginComponent },
   { path: 'signup', component: SignUpPageComponent },
   { path: 'users/:id/contacts', component: UserDetailsComponent },
]

export const routing = RouterModule.forRoot(routes)
