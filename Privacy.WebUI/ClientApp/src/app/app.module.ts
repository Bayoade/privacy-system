import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { routing } from 'src/app/app.routing';
import { HttpModule } from '@angular/http';
import { ModalModule } from 'ngx-bootstrap';

import { AppComponent } from './app.component';
import { ReactiveFormsModule } from '@angular/forms';
import { EditUserComponent } from 'src/app/components/user/edit/edit.component';
import { UserListComponent } from 'src/app/components/user/list/index.component';
import { NavBarComponent } from 'src/app/widget/navBar/nav.bar.component';
import { FooterComponent } from 'src/app/widget/footer/footer.component';
import { LoginComponent } from 'src/app/components/auths/login/login.component';
import { SignUpPageComponent } from 'src/app/components/auths/signup/signup.page.component';

import { UserProfileService } from 'src/app/common/services/userprofile.service';
import { UserDetailsComponent } from 'src/app/components/user/userdetails/user-details-component';
import { AccountService } from 'src/app/common/services/account.service';
import { ContactService } from 'src/app/common/services/contact.service';
import { DataService } from 'src/app/common/shared/data.service';
import { DatePipe } from '@angular/common';

@NgModule({
  declarations: [
     AppComponent,
     EditUserComponent,
     UserListComponent,
     NavBarComponent,
     FooterComponent,
     SignUpPageComponent,
     LoginComponent,
     UserDetailsComponent,

  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    BrowserModule,
    ReactiveFormsModule,
    routing,
    HttpModule,
    ModalModule.forRoot(),
  ],
  providers: [
    UserProfileService,
    AccountService,
    ContactService,
    DataService,
    DatePipe,
  ],
  bootstrap: [
    AppComponent
  ],
  entryComponents: []
})
export class AppModule { }
