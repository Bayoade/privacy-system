import { Component, OnInit, ViewChild} from '@angular/core'
import { NavBarComponent } from 'src/app/widget/navBar/nav.bar.component';
import { LoginComponent } from 'src/app/components/auths/login/login.component';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent{

  constructor(){ }
}