import { Http } from "@angular/http";
import { Observable, empty } from "rxjs";
import { retry, map, catchError } from "rxjs/operators";
import { RegistrationModel } from "src/app/common/entities/registration.model";
import { LoginUserModel } from "src/app/common/entities/login.user.model";
import { Injectable } from "@angular/core";
import { SaveUserProfileModel } from "src/app/common/entities/saveuserprofile.model";
import { NamedId } from "src/app/common/entities/namedId";

@Injectable()
export class AccountService {
   private BASE_URL: string = "http://localhost:19479/api/accounts/";

   constructor(private _http: Http){}

   manageLogin(user: LoginUserModel) : Observable<NamedId>{
      return this._http.post(this.BASE_URL + 'login', user).pipe(
         retry(3),
      map((res: Response) => res.json()),
      catchError((error: any, caught: any) => {
          return empty();
      }) 
      )
   }

   createUser(newUser: RegistrationModel) : Observable<string>{
    return this._http.post(this.BASE_URL, newUser).pipe(
        map((res: Response) => res.json()),
        catchError((error: any, caught: any) => {
            return empty();
        })
    )}

    UpdateUserProfile(id: string, userProfileViewModel : SaveUserProfileModel) : Observable<void>{
        return this._http.put(this.BASE_URL + `${id}`, userProfileViewModel).pipe(
            map((res: Response) => res.json()),
            catchError((error: any, caught: any) => {
                return empty();
            })
        )
    }

    DeleteUserProfile(id : string): Observable<boolean>{
        return this._http.delete(this.BASE_URL + `${id}`).pipe(
            map((res: Response) => true),
            catchError((error: any, caught: any) => {
                return empty();
            })
        )
    }

    getUserName(userId: string) : Observable<string>{
        return this._http.get(this.BASE_URL + `${userId}/username`).pipe(
            retry(3), 
            map((res: Response) => res.text()),
            catchError((error: any, caught: any) => {
                return empty();
            })  
        )
    }
}