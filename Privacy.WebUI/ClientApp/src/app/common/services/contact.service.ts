import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { retry, map, catchError } from "rxjs/operators";
import { empty, Observable } from "rxjs";
import { ContactProfileModel } from "src/app/common/entities/contact.profile.model";

@Injectable()
export class ContactService{
   private BASE_URL:string = 'http://localhost:19479/api/userprofiles/'

   constructor(private _http: Http){
   }

   getAllContactProfiles(id: string) : Observable<ContactProfileModel[]>{
      return this._http.get(`${this.BASE_URL + id}/contacts`).pipe(
         retry(3),
         map(res =>res.json()),
         catchError((error: any, caught: any) => {
            return empty();
        })
      )
   }

   deleteContactProfile(userId: string, contactId: string): Observable<boolean>{
      return this._http.delete(`${this.BASE_URL + userId}` + `/contacts/${contactId}`).pipe(
         map(res => true),
         catchError((error: any, caught: any) => {
            return empty();
         })
      )
   }

   addContactProfile(userId: string, contact: ContactProfileModel): Observable<string>{
    return this._http.post(`${this.BASE_URL + userId}/contacts`, contact).pipe(
      map(res => res.json()),
      catchError((error: any, caught: any) => {
         return empty();
      })
    )
  }

  updateContact(userId: string, contactProfile: ContactProfileModel): Observable<boolean>{
    return this._http.put(`${this.BASE_URL + userId}/contacts/${contactProfile.id}`, contactProfile).pipe(
      map(res => true),
      catchError((error: any, caught: any) => {
         return empty();
      })
    )
  }

}