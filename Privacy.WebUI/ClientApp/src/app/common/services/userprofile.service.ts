import { Injectable } from "@angular/core"
import { Http } from "@angular/http";
import { Observable, empty } from "rxjs";
import { map, catchError, retry } from "rxjs/operators";
import { UserProfileModel } from "src/app/common/entities/userprofile.model";
import { SaveUserProfileModel } from "src/app/common/entities/saveuserprofile.model";

@Injectable()
export class UserProfileService{
   private BASE_URL:string = 'http://localhost:19479/api/userProfiles/';

   constructor(
      private _http: Http
   ){ }

   getAllUserProfiles() : Observable<UserProfileModel[]>{
      return this._http.get(this.BASE_URL).pipe(
            retry(3),
            map((res: Response) => res.json()),
            catchError((error: any, caught: any) => {
                return empty();
            })  
        )
    }

    getUserProfile(userId: string) : Observable<UserProfileModel>{
        return this._http.get(this.BASE_URL + `${userId}`).pipe(
            retry(3), 
            map((res: Response) => res.json()),
            catchError((error: any, caught: any) => {
                return empty();
            })  
        )
    }
}