export class SaveUserProfileModel{
   firstName: string;
   lastName: string;
   middleName: string;
   email: string;
   address: string;
   phoneNumber: string;
   dateOfBirth: Date;
   emergencyNumber: string;
}