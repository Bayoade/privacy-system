export class LoginUserModel{
 constructor(
  public email?: string,
  public password?: string)
  {
    this.email = this.email || "",
    this.password = this.password || ""
  }
}