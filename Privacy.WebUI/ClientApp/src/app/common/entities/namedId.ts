export class NamedId{
   constructor(
      public id: string,
      public name: string
   ){
      this.id =  this.id || "",
      this.name = this.name || ""
   }
}