export class RegistrationModel{
   constructor(
      public firstName?: string,
      public lastName?: string,
      public middleName?: string,
      public email?: string,
      public address?: string,
      public phoneNumber?: string,
      public dateOfBirth?: any,
      public emergencyNumber?: string,
      public password?: string,
   ){
      this.firstName = this.firstName || "",
      this.lastName = this.lastName || "",
      this.middleName = this.middleName || "",
      this.email = this.email || "",
      this.address = this.address || "",
      this.phoneNumber = this.phoneNumber || "",
      this.dateOfBirth = this.dateOfBirth || "",
      this.emergencyNumber = this.emergencyNumber || "",
      this.password = this.password || ""
   }
   
   
}