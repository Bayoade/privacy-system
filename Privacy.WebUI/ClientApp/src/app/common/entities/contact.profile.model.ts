import {formatDate} from '@angular/common';

export class ContactProfileModel{
   constructor(
      public id?:string,
      public firstName?: string,
      public lastName?: string,
      public middleName?: string,
      public email?: string,
      public address?: string,
      public phoneNumber?: string,
      public dateOfBirth?: any,
      public emergencyNumber?: string,
      public name?: string,
      public website?: string,
   ){
      this.id = this.id || "";
      this.firstName = this.firstName || "";
      this.lastName = this.lastName || "";
      this.middleName = this.middleName || "";
      this.email = this.email || "";
      this.address = this.address || "";
      this.phoneNumber = this.phoneNumber || "";
      this.dateOfBirth = this.dateOfBirth || "";
      this.emergencyNumber = this.emergencyNumber || "";
      this.name = this.name || "";
      this.website = this.website || "";
   }
}