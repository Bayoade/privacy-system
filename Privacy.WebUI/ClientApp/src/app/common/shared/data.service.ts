import { Injectable } from "@angular/core";
import { Subject, BehaviorSubject } from "rxjs";
import { ContactProfileModel } from "src/app/common/entities/contact.profile.model";

@Injectable()
export class DataService{
    loggedInUser: string;
    id: string;
    contactProfiles$: ContactProfileModel[];
}