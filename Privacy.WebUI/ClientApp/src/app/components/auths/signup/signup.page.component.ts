import { OnInit, Component } from "@angular/core";
import { RegistrationModel } from "src/app/common/entities/registration.model";
import { UserProfileService } from "src/app/common/services/userprofile.service";
import { UserProfileModel } from "src/app/common/entities/userprofile.model";
import { AccountService } from "src/app/common/services/account.service";

@Component({
   selector: 'signup-page-component',
   templateUrl: './signup.page.html'
})

export class SignUpPageComponent{
   userProfile: UserProfileModel;
   confirmPassword: string;
   signUpUser: RegistrationModel = new RegistrationModel()

   constructor(private _accountService: AccountService){

   }
   register(): void {
      if(this.confirmPassword == this.signUpUser.password){
         this._accountService.createUser(this.signUpUser).subscribe(res => {
            this.userProfile = new UserProfileModel(
               res, 
               this.signUpUser.firstName,
               this.signUpUser.lastName,
               this.signUpUser.middleName,
               this.signUpUser.email,
               this.signUpUser.address,
               this.signUpUser.phoneNumber,
               this.signUpUser.dateOfBirth,
               this.signUpUser.emergencyNumber)
         });
      }
   }
}