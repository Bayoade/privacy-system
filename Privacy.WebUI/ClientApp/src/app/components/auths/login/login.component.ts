import { Component, OnInit, TemplateRef, Output} from '@angular/core';
import { LoginUserModel } from 'src/app/common/entities/login.user.model';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { Route, Router } from '@angular/router';
import { EventEmitter } from 'events';
import { AccountService } from 'src/app/common/services/account.service';
import { DataService } from 'src/app/common/shared/data.service';

@Component({
  selector: 'login-component',
  templateUrl: './login.component.html'
})

export class LoginComponent implements OnInit{
  loginUser: LoginUserModel = new LoginUserModel();
  title: string;
  body: string;
  
  modalRef: BsModalRef;

  constructor(
    private _accountService: AccountService,
    private _modalService: BsModalService,
    private _router: Router,
    private _dataService: DataService,
  ){}

  ngOnInit(){

  }

  get loggedInUser():string { 
      return this._dataService.loggedInUser; 
    } 
   set loggedInUser(value: string) { 
      this._dataService.loggedInUser = value; 
    } 

    get id(): string{
      return this._dataService.id;
    }
    set id(value: string){
       this._dataService.id = value;
    }

  submit(template: TemplateRef<any>){
    this._accountService.manageLogin(this.loginUser).subscribe(res => {
      if(!res){
        this.title = `Information`;
        this.body = `The user email or password is incorrect`;
        return this.modalRef = this._modalService.show(template);
      }

      this.loggedInUser = res.name;
      this.id = res.id;
      this._router.navigate([`users/${this.id}/contacts`]);
      
    });
  }
}