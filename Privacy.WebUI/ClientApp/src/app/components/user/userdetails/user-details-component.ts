import { OnInit, Component, TemplateRef, ViewChild, ElementRef} from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { UserProfileService } from "src/app/common/services/userprofile.service";
import { BsModalService, BsModalRef } from "ngx-bootstrap";
import { ContactProfileModel } from "src/app/common/entities/contact.profile.model";
import { AccountService } from "src/app/common/services/account.service";
import { ContactService } from "src/app/common/services/contact.service";
import { DataService } from "src/app/common/shared/data.service";
import * as $ from 'jquery';
import 'datatables.net';
import { DatePipe } from "@angular/common";

@Component({
   selector: "user-details-component",
   templateUrl: "user-details-component.html"
})

export class UserDetailsComponent implements OnInit {
   public contactId: string;
   public title: string;
   public body: string;
   public name: string;
  //  public contactProfiles$: ContactProfileModel[];
   public contactProfile: ContactProfileModel = new ContactProfileModel();
   
   public modalRef: BsModalRef;

   @ViewChild('dataTable') table: ElementRef;
   private dataTable: any;
   private dtSettings: object = {
    "paging":   true,
    "ordering": true,
    "info":     true,
  }

   constructor(
      private _route: ActivatedRoute,
      private _modalService: BsModalService,
      private _accountService: AccountService,
      private _contactService: ContactService,
      private _dataService: DataService,
      private _router: Router,
      private _datePipe: DatePipe){

        this._initDatatable();
       }

  ngOnInit(){
    this.getUserId();
    this.getUserName();
    this.loadContactProfiles();
  }

  openModalContact(template: TemplateRef<any>): void {
    this.title = 'Add New Contact'
    if(this.contactProfile.id){
       this.title =`Are you sure you want to edit ${this.contactProfile.name}?`;
    }
    this.modalRef = this._modalService.show(template, {class: 'modal-xl'} );
 }

 addContact(userId){
  this._contactService.addContactProfile(userId, this.contactProfile)
  .subscribe(res => {
     this.contactProfile.id = res;
     this.dataTable;
     this.contactProfiles$.push(this.contactProfile);
     this.modalRef.hide();
     this.reloadCurrentPage()
   })
 }

 updateContact(userId: string, contactProfile: ContactProfileModel){
    this._contactService.updateContact(userId, contactProfile).subscribe(res => {
      this.modalRef.hide();
      this.reloadCurrentPage()
    })
 }

 saveContact(){
   let userId = this._dataService.id;
   if(!this.contactProfile.id){
    return this.addContact(userId)
   }
   this.updateContact(userId, this.contactProfile)
 }

 reloadCurrentPage(){
  window.location.reload();
 }

   get contactProfiles$():ContactProfileModel[] { 
    return this._dataService.contactProfiles$; 
  } 
  set contactProfiles$(value: ContactProfileModel[]) {
    this._dataService.contactProfiles$ = value; 
  } 

  get loggedInUser():string { 
    return this._dataService.loggedInUser; 
  } 
  set loggedInUser(value: string) {
    this._dataService.loggedInUser = value; 
  } 

  get id(): string{
    return this._dataService.id;
  }
  set id(value: string){
      this._dataService.id = value;
  }

  private _initDatatable(): void {
    setTimeout(()=>{
      this.dataTable = $(this.table.nativeElement);
      this.dataTable.DataTable(this.dtSettings);
    }, 2000);
  }

   getUserName(){
      if(this.id){
         this._accountService.getUserName(this.id).subscribe(res => {
            this.loggedInUser = res;;
         })
      }
      //this._router.navigate([`/login`]);
   }

   getUserId(){
      this._route.params.subscribe(params => {
         this.id = params['id'];
      });
   }
 
   refreshModal(){
     this.contactId = null;
     this.name = null;
     this.modalRef = null;
   }
 
   openModal(id: string, name: string, template: TemplateRef<any>) {
     this.title = `Confirm Delete`;
     this.body = `Do you want to delete this user ${name}?`;
     this.contactId = id;
     this.name = name;
     this.modalRef = this._modalService.show(template);
   }
 
   confirm(){
      this._contactService.deleteContactProfile(this.id, this.contactId).subscribe(res => {
         let index = this.contactProfiles$.findIndex(x => x.id == this.contactId);
         if(~index){
           this.contactProfiles$.splice(index, 1);
         }
         this.modalRef.hide();
         this.refreshModal();
         this.reloadCurrentPage()
      })
   }
 
   decline(){
     this.modalRef.hide();
     this.refreshModal()
   }
 
   loadContactProfiles() : void {
     this._contactService.getAllContactProfiles(this.id).subscribe( res => {
       this.contactProfiles$ = res;
       error => console.log(error);
     })
   }

   editUserProfile(id: string, template: TemplateRef<any>){
      this.contactProfile = this.contactProfiles$.find(x => x.id == id);
      this.contactProfile.dateOfBirth = this._datePipe.transform(this.contactProfile.dateOfBirth,"yyyy-MM-dd");
      this.openModalContact(template);
   }
}