import { Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import { Subject } from 'rxjs';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { map } from 'rxjs/internal/operators/map';
import { catchError } from 'rxjs/internal/operators/catchError';
import { UserProfileService } from 'src/app/common/services/userprofile.service';
import { UserProfileModel } from 'src/app/common/entities/userprofile.model';
import { AccountService } from 'src/app/common/services/account.service';

@Component({
  templateUrl: './index.component.html'
})

export class UserListComponent implements OnInit{
  userProfiles: UserProfileModel[];
  userProfile: UserProfileModel;
  
  title: string;
  body: string;
  id: string;
  name: string;

  modalRef: BsModalRef;

  constructor(
    private _productService : UserProfileService,
    private _modalService: BsModalService,
    private _accountService: AccountService ){}

  ngOnInit() {
    this.loadUserProfiles();
  }

  refreshModal(){
    this.id = null;
    this.name = null;
    this.modalRef = null;
  }

  openModal(id: string, name: string, template: TemplateRef<any>) {
    this.title = `Confirm Delete`;
    this.body = `Do you want to delete this user ${name}?`;
    this.id = id;
    this.name = name;
    this.modalRef = this._modalService.show(template);
  }

  confirm(){
    this._accountService.DeleteUserProfile(this.id).subscribe(res => {
        let index = this.userProfiles.findIndex(x => x.id == this.id);
        if(~index){
          this.userProfiles.splice(index, 1);
        }
        this.modalRef.hide();
        this.refreshModal();
     })
  }

  decline(){
    this.modalRef.hide();
    this.refreshModal()
  }

  loadUserProfiles() : void {
    this._productService.getAllUserProfiles().subscribe( res => {
      this.userProfiles = res;
      error => console.log(error);
    })
  }
}