import { OnInit, Component, Input } from "@angular/core";
import { AccountService } from "src/app/common/services/account.service";
import { ActivatedRoute, Router } from "@angular/router";
import { DataService } from "src/app/common/shared/data.service";
@Component({
   selector: 'nav-bar-component',
   templateUrl: './nav.bar.html'
})
export class NavBarComponent{
   constructor(
      private _dataService: DataService,
      private _router: Router){ }

   get loggedInUser():string { 
      return this._dataService.loggedInUser; 
    } 
   set loggedInUser(value: string) { 
      this._dataService.loggedInUser = value; 
    } 

    get id(): string{
      return this._dataService.id;
    }
    set id(value: string){
       this._dataService.id = value;
    }

    getAllContacts(){
      this._router.navigate([`users/${this.id}/contacts`])
    }

    logOut(){
       this._dataService.id = null;
       this._dataService.loggedInUser = null;
    }
}