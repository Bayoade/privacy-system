﻿using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace Privacy.Core.Helpers
{
    public static class CryptoExtension
    {

        public static string ToSha256(this string password, string salt = null, int iterations = 1)
        {
            // derive a 256-bit subkey (use HMACSHA1 with 10,000 iterations)
            if (salt != null)
            {
                password += salt;
            }

            byte[] byteSalt = Encoding.ASCII.GetBytes(salt);

            string hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: password,
                salt: byteSalt,
                prf: KeyDerivationPrf.HMACSHA1,
                iterationCount: iterations,
                numBytesRequested: 256 / 8));

            return hashed;
        }

        public static string GenerateSalt(int maxLength)
        {
            //  create salt from max length
            var salt = new byte[maxLength];

            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(salt);
            }

            return Convert.ToBase64String(salt);
        }
    }
}
