﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Privacy.Core.Models
{
    public class NamedId<T> 
    {
        public NamedId()
        {

        }

        public T Id { get; set; }

        public string Name { get; set; }
    }
}
