﻿namespace Privacy.Core.Models
{
    public class NamedPhoneNumberId<T>
    {
        public NamedPhoneNumberId()
        {

        }
        public T Id { get; set; }

        public string Name { get; set; }

        public string PhoneNumber { get; set; }
    }
}
