﻿using System;
using static Privacy.Core.Helpers.ExtensionMethods;

namespace Privacy.Core.Models
{
    public class UserProfile
    {
        private string _name;

        public Guid Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string MiddleName { get; set; }

        public string Email { get; set; }

        public string Address { get; set; }

        public string PhoneNumber { get; set; }

        public DateTime DateOfBirth { get; set; }

        public string EmergencyNumber { get; set; }

        public string Name
        {
            get
            {
                if (_name.IsNull())
                {
                    var middleName = MiddleName.IsNull() ? string.Empty : MiddleName;
                    _name = $"{FirstName} {middleName} {LastName}";
                }

                return _name;
            }
        }

        public string Password { get; set; }

        public string Salt { get; set; }
    }
}
