﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Privacy.Core.Models
{
    public class LoginProfile
    {
        public string Email { get; set; }

        public string Password { get; set; }
    }
}
