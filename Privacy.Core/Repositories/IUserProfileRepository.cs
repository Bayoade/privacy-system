﻿using Privacy.Core.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Privacy.Core.Repositories
{
    public interface IUserProfileRepository
    {
        Task<UserProfile> GetUserProfileAsync(Guid userId);

        Task<IList<UserProfile>> GetAllUserProfilesAsync();

        Task AddUserProfileAsync(UserProfile user);

        Task RemoveUserProfileAsync(params Guid[] userIds);

        Task UpdateUserProfileAsync(UserProfile user);

        Task<UserProfile> FindUserByEmail(string email);
    }
}
