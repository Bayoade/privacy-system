﻿using Privacy.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Privacy.Core.Repositories
{
    public interface IContactRepository
    {
        Task AddContactsToUserAsync(Guid userId, ContactProfile contactProfile);

        Task RemoveContactFromUserAsync(Guid userId, params Guid[] ids);

        Task UpdateContactInUserAsync(ContactProfile contactProfile);

        Task DeleteUserContactAsync(Guid userId, params Guid[] ids);

        Task<IList<ContactProfile>> GetAllUserContactsAsync(Guid userId);

        Task<ContactProfile> GetContactToUserProfileAsync(Guid userId, Guid id, bool allowNull = false);

        Task DeleteAllUserContacts(Guid userId);
    }
}
