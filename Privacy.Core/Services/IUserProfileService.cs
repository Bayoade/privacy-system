﻿using Privacy.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Privacy.Core.Services
{
    public interface IUserProfileService
    {
        Task<UserProfile> GetUserProfileAsync(Guid id);

        Task<IList<UserProfile>> GetAllUserProfilesAsync();

        Task RemoveUserAsync(Guid id);

        Task UpdateUserAsync(UserProfile userProfile);

        Task<Guid> AddUserProfileAsync(UserProfile userProfile);
    }
}
