﻿using Privacy.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Privacy.Core.Services
{
    public interface IContactService
    {
        Task<ContactProfile> GetContactToUserProfileAsync(Guid userId, Guid id);

        Task<IList<ContactProfile>> GetAllUserContactsProfileAsync(Guid userId);

        Task RemoveContactFromUserProfileAsync(Guid userId, params Guid[] contactIds);

        Task UpdateContactInUserProfileAsync(ContactProfile contactProfile);

        Task<Guid> AddContactToUserProfileAsync(Guid userId, ContactProfile contactProfile);

        Task DeleteUserContactAsync(Guid userId, params Guid[] ids);
    }
}
