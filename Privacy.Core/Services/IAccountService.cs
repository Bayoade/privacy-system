﻿using Privacy.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Privacy.Core.Services
{
    public interface IAccountService
    {
        Task<NamedId<Guid>> AuthenticateLogin(LoginProfile model);

        Task<string> GetUserNameAsync(Guid userId);
    }
}
